<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		div.solid {border-style: solid;}
		div.padding {padding: 5%;}
	</style>
	<title>PHP S1 - Activity</title>
</head>
<body>
	<div class="solid padding">
		<h1>Full Address</h1>
		<p><?php echo getFullAddress("Philippines", "Metro Manila", "Quezon City", "3F Caswynn Bldg., Timog Avenue") ?></p>
		<p><?php echo getFullAddress("Philippines", "Metro Manila", "Makati City", "3F Enzo Bldg., Buendia Avenue") ?></p>

	</div>
	<br>
	<div class="solid padding">
		<h1>Letter-Based Grading</h1>
		<p><?php echo getLetterGrade(87); ?></p>
		<p><?php echo getLetterGrade(94); ?></p>
		<p><?php echo getLetterGrade(74); ?></p>
	</div>
</body>
</html>