<?php 

function getFullAddress($country, $city, $province, $specificAdress) {
	return "$specificAdress, $city, $province, $country";
}

function getLetterGrade($grade) {
	if($grade < 75) {
		return "$grade is equivalent D";
	}else if($grade <= 76) {
		return "$grade is equivalent C-";
	}else if($grade <= 79) {
		return "$grade is equivalent C";
	}else if($grade <= 82) {
		return "$grade is equivalent C+";
	}else if($grade <= 85) {
		return "$grade is equivalent B-";
	}else if($grade <= 88) {
		return "$grade is equivalent B";
	}else if($grade <= 91) {
		return "$grade is equivalent B+";
	}else if($grade <= 94) {
		return "$grade is equivalent A-";
	}else if($grade <= 96) {
		return "$grade is equivalent A";
	}else if($grade <= 100) {
		return "$grade is equivalent A+";
	}else {
		return "Out of scope";
	}
}

?>