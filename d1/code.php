<?php

/*
When writing any PHP code, always remember to start with the opening php tag

<?php

Closing with ?> is optional when ONLY writing PHP code, but later we will use this
*/

//echo is used to output data to the screen
//always remember to end every PHP statement with a semicolon (;)
//echo "Mabuhay";

//[SECTION] Variables

$name = "John Smith";
$email = "johnsmith@mail.com";

//[SECTION] Constants

define('PI', 3.1416);

//[SECTION] Data Types
$state = "New York";
$country = "USA";
$address = $state . ', ' . $country; //concatenation via (.)
$adress2 = "$state, $country"; //concatenation via ("")

//Integers
$age = 31;
$headcount = 26;

//Floats
$grade = 98.2;
$distance = 1342.12;

//Boolean
$has_travelled_abroad = true;

//Null
$spouse = null;

//Arrays
$grades = array(98.7, 92.1, 90.2,94.6);

//Objects
$gradesObj = (object) [
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6,
];

$personObj = (object)[
	'fullName' => "John Smith",
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => "New York",
		'country' => "USA",
	],
];

//[SECTION] Operators

//Assignment Operator (=)
$x = 234;
$y = 123;

$isLegalAge = true;
$isRegistered = false;

//[SECTION] Functions

function get_full_name($first_name, $middle_initial, $last_name) {
	return "$last_name, $first_name $middle_initial";
}

//[SECTION] Selection Control Structures (Conditional Statements)

function determine_typhoon_intensity($wind_speed) {
	if($wind_speed < 30) {
		return "Not a typhoon yet";
	}else if($wind_speed <= 60) {
		return "Tropical depression detected.";
	}else if($wind_speed >= 62) {
		return "Tropical storm detected";
	}else if($wind_speed >= 89 && $wind_speed <= 117) {
		return "Severe tropical storm detected";
	} else {
		return "Typhoon detected";
	}
}

//Ternary Operator
function is_under_age($age) {
	return ($age < 18) ? true : false;
}

//Switch Statement
function determine_user($computer_number) {
	switch($computer_number) {
		case 1:
			return "Linus Torvalds";
			break;
		case 2:
			return "Steve Jobs";
			break;
		case 3:
			return "Sid Meier";
			break;
		default:
			return "The computer number $computer_number is out of bounds.";
			break;
	}
}