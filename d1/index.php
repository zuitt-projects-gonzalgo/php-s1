<!-- PHP code can be included in another file by using the require_once keyword. -->
<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S01</title>
</head>
<body>
	<!-- variables can be used to output data in double quites while single quotes will not work -->
	<h1>Variables</h1>
	<p><?php echo "Gooday $name! Your email is $email"; ?></p>
	<p><?php echo PI; ?></p>
	<p><?php echo $address; ?></p>

	<h1>Data Types</h1>
	<!-- Normal echoing of boolean and null variables will not make them visible to the user. To see their types instead, we can use the following: -->
	<p><?php echo gettype($spouse); ?></p>
	<!-- To see more detailed info on the variable, use var_dump -->
	<p><?php var_dump($has_travelled_abroad); ?></p>
	<p><?php var_dump($grades); ?></p>
	<p><?php print_r($grades); ?></p>
	<p><?php print_r($personObj); ?></p>
	
	<h1>Operators</h1>
	<h3>Arithmetic Operators</h3>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Quotient: <?php echo $x / $y; ?></p>

	<h3>Equality Operattors</h3>
	<p>Loose Equality: <?php var_dump($x == 234); ?></p>
	<p>Loose inequality: <?php var_dump($x != 234); ?></p>
	<p>Strict Equality: <?php var_dump($x === 123); ?></p>
	<p>Strict Inequality: <?php var_dump($x !== 234); ?></p>

	<h3>Greater/Lesser Operators</h3>
	<p>Is Lesser (or equal): <?php var_dump($x <= $y); ?></p>
	<p>Is Greater (or equal): <?php var_dump($x >= $y); ?></p>

	<h3>Logical Operators</h3>
	<p>Are all requirements met: <?php var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Are some requirements met: <?php var_dump($isLegalAge || $isRegistered); ?></p>
	<p>Are some requirements not met: <?php var_dump(!$isLegalAge && !$isRegistered); ?></p>

	<h1>Functions:</h1>
	<p>Full Name: <?php echo get_full_name("John", "J.", "Smith"); ?></p>

	<h1>Selection Control Structure</h1>
	<h3>If-Elseif-Else</h3>
	<p><?php echo determine_typhoon_intensity(117); ?></p>

	<h3>Ternary Operator</h3>
	<p><?php var_dump(is_under_age(78)); ?></p>
	<p><?php var_dump(is_under_age(13)); ?></p>

	<h3>Switch Statement</h3>
	<p><?php echo determine_user(2); ?></p>
</body>
</html>